import os
import numpy as np
import matplotlib.pyplot as plt

from PIL import Image, ImageFont, ImageDraw


if __name__ == '__main__' :
    dir = os.getcwd().replace(os.sep, '/') + '/images'
    print(dir)

    label_font = ImageFont.truetype('arial_narrow_7.ttf', 100)

    imcount = len(os.listdir(dir))
    ims = []
    for filename in os.listdir(dir):
        print(filename[:-4])
        im_full = Image.open(dir + '/' + filename)
        im = im_full.resize((im_full.width//4, im_full.height//4))
        im = im.rotate(90, expand=1) ## Modify

        fig, ax = plt.subplots()
        class HorizonTracker(object):
            def __init__(self, input_image):
                ax.set_title('Move horizon line up or down using the arrow keys \n Move to next image by closing')

                self.im = input_image
                self.horizon_height = self.im.height/2

                self.im_display = ax.imshow(self.im)
                self.horizon_display, = ax.plot([0, self.im.width], [self.horizon_height, self.horizon_height], 'r', lw=2)
                self.update()

            def on_key(self, event):
                #print('you pressed', event.key, event.xdata, event.ydata)
                if event.key == 'up':
                    self.horizon_height -= 10
                    self.update()
                elif event.key == 'down':
                    self.horizon_height += 10
                    self.update()

            def on_click(self, event):
                self.horizon_height = int(event.ydata)
                self.update()

            def update(self):
                self.horizon_display.set_ydata([self.horizon_height, self.horizon_height])
                plt.draw()



        tracker = HorizonTracker(im)
        fig.canvas.mpl_connect('key_press_event', tracker.on_key)
        fig.canvas.mpl_connect('button_press_event', tracker.on_click)
        plt.show()

        image_edit = ImageDraw.Draw(im)
        image_edit.text((15,15), filename[:-4], (252, 252, 3), font=label_font)
        # im.show()

        new_im = im.crop((0, 0, im.width, tracker.horizon_height))
        new_im.save(filename[:-4] + '_cropped.jpg')
        ims.append(new_im)

    heights = [x.height for x in ims]
    max_h = max(heights)
    width = ims[-1].width
    combined_im = Image.new('RGB', (imcount*(30+width), max_h), (255, 255, 255))

    for i in range(imcount):
        x_start = i*(30+width)
        x_end = x_start + width

        y_start = max_h-heights[i]
        y_end = max_h

        combined_im.paste(ims[i], (x_start, y_start))

    # combined_im.show()
    combined_im.save('combined.jpg')



