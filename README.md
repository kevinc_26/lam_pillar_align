# README #

LAM Pillar Align is used for creating combined labeled images cropped to a horizon line for placing into presentations

### How do I get set up and use the script? ###

* Navigate to a destination folder in terminal and run <br /><code>git clone https://kevinc_26@bitbucket.org/kevinc_26/lam_pillar_align.git</code>
* Paste the images you want to crop together in the 'images' folder
  * If images folder does not exist within 'lam_pillar_align', add one
* Run the script
  * Each image will open. Click to place the horizon, use the up/down arrow keys for fine adjustment, use the right arrow key to rotate
  * Each individual image will be saved - cropped and with text added
  * The combined image will be saved as 'combined.jpg'
  

### Who do I talk to? ###

* Kevin